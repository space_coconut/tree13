#include <iostream>
#include <tree13.h>
#include <tree13.cpp>
#include <ctime>

int main()
{
    srand(time(NULL));

    Tree <int> D;

    for(size_t i=0; i<10; i++)
         D.build_tree(rand()%10);

    std::cout<< "Show tree:" << std::endl;
    D.show_tree();

    std::cout<< std::endl << "Min. element: ";

    D.min_search();

    std::cout<< std::endl << "Max. element: ";

    D.max_search();

    std::cout<< std::endl << "Root: ";

    std::cout<< D.get_Root()->data << std::endl << std::endl << "Delete subtree. Enter element (not a root): ";

    int x;
    std::cin>> x;

    std::cout<< "Tree without subtree:" << std::endl;

    D.remove_subtree(x);

    D.show_tree();

    int y;
    std::cout<< std::endl << "Delete nood. Enter element: ";
    std::cin>> y;

    std::cout<< "Tree without element:" << std::endl;

    D.remove_node(y);

    D.show_tree();

    return 0;

}
