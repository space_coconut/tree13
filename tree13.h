#ifndef TREE13_H
#define TREE13_H
#include <iostream>

template <class T>
class Tree{
    struct Node{
        T data;
        size_t level = 0;
        Node* Parent;
        Node* Right;
        Node* Left;
        Node();
        Node(const T& b, Node* a);
    };
    Node* Root;

    void h_show_tree(Node* a, size_t Level)const;

    Node* min_search_h(Node* Root){
        Node* search = Root;
        if(Root->Left != nullptr)
            min_search_h(Root->Left);
        else {
            std::cout<< search->data << std::endl;
            return search;
        }
        return nullptr;
    }

    Node* max_search_h(Node* Root){
        Node* search = Root;
        if(Root->Right != nullptr)
            max_search_h(Root->Right);
        else {
            std::cout<< search->data << std::endl;
            return search;
        }
        return nullptr;
    }

    Node* search(const T& x, Node* a){
        while(a != nullptr){
            if(a->data == x)
                return a;
            else{
                if(a->data > x)
                    return search(x, a->Left);
                else return search(x, a->Right);
            }
        }
        return nullptr;
    }

    void delete_tree(Node* i);

    void remove_node_h(Node* Nodee, const T& x);

public:
    Tree();

    void add(const T& j, Node* Root);

    void build_tree(const T& a);

    void add_Node(const T& Value);

    void show_tree();

    Node* min_search(){
        return min_search_h(Root);
    }

    Node* max_search(){
        return max_search_h(Root);
    }

    Node* get_Root(){
        return Root;
    }

    void remove_subtree(const T& x);

    void remove_node(const T& x);
};

#endif // TREE13_H
