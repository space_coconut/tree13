#include "tree13.h"

template <class T>
Tree<T>::Tree(){
    Root = nullptr;
}

template <class T>
void Tree<T>::add(const T& j, Node* Root){
    if(Root->data > j){
        if(Root->Left == nullptr)
            Root->Left =new Node(j, Root);
        else add(j, Root->Left);
    }

    if(Root->data < j){
        if(Root->Right == nullptr)
            Root->Right =new Node(j, Root);
        else add(j, Root->Right);
    }
}

template <class T>
void Tree<T>::build_tree(const T& a){
    add_Node(a);
}

template <class T>
void Tree<T>::add_Node(const T& Value){
    Node* Paren = nullptr;
    if(Root == nullptr)
        Root =new Node(Value, Paren);
    else add(Value, Root);
}

template <class T>
Tree<T>::Node::Node(){
    data = nullptr;
    Parent = nullptr;
    Right = nullptr;
    Left = nullptr;
}


template <class T>
Tree<T>::Node::Node(const T &b, Node *a){
    data = b;
    Parent = a;
    Right = nullptr;
    Left = nullptr;
}

template <class T>
void Tree<T>::h_show_tree(Node* a, size_t Level)const{
    if(a != nullptr){
        h_show_tree(a->Left, Level+1);
        for(size_t i=0; i<Level; i++)
            std::cout<< ".";
        std::cout<< "(" << a->data << ")" << std::endl;
        h_show_tree(a->Right, Level+1);
    }
}

template <class T>
void Tree<T>::show_tree(){
    if(Root != nullptr)
        h_show_tree(Root, Root->level);
    else throw "Tree is empty";
}

template <class T>
void Tree<T>::delete_tree(Node* i){
    if(i != nullptr){
        if(i->Left != nullptr)
            delete_tree(i->Left);
        if(i->Right != nullptr)
            delete_tree(i->Right);
        delete i;
    }
}

template <class T>
void Tree<T>::remove_subtree(const T& x){
    Node* temp = search(x, Root);
    if(Root->data != x){
        if(temp != nullptr){
            Node* Parent = temp->Parent;
            if(Parent != nullptr)
                if(Parent->Left == temp)
                    Parent->Left = nullptr;
                else if(Parent->Right == temp)
                    Parent->Right = nullptr;
        }
        if(temp != nullptr){
            delete_tree(temp->Left);
            delete_tree(temp->Right);
            delete temp;
        }
    }
}

template <class T>
void Tree<T>::remove_node_h(Node* Nodee, const T& x){
    Node* Pointer = Nodee;
    Node* Parent = Nodee->Parent;
    while(Pointer != nullptr && Pointer->data != x){
        Parent = Pointer;
        if(x < Pointer->data)
            Pointer = Pointer->Left;
        else Pointer = Pointer->Right;
    }
    if(Pointer != nullptr){
        Node* Removed = nullptr;
        if(Pointer->Left == nullptr || Pointer->Right == nullptr){
            Node* Child = nullptr;
            Removed = Pointer;
            if(Pointer->Left != nullptr)
                Child = Pointer->Left;
            else if(Pointer->Right != nullptr)
                Child = Pointer->Right;
            if(Parent == nullptr)
                Root = Child;
            else {
                if(Parent->Left == Pointer)
                    Parent->Left = Child;
                else Parent->Right = Child;
            }
        }
        else {
            Node* mostLeft = Pointer->Right;
            Node* mostLeftParent = Pointer;
            while(mostLeft->Left !=nullptr){
                mostLeftParent = mostLeft;
                mostLeft = mostLeft->Left;
            }
            Pointer->data = mostLeft->data;
            Removed = mostLeft;
            if(mostLeftParent->Left == mostLeft)
                mostLeftParent->Left = nullptr;
            else mostLeftParent->Right = mostLeft->Right;
        }
        delete Removed;
    }
}

template <class T>
void Tree<T>::remove_node(const T& x){
    return remove_node_h(Root, x);
}
